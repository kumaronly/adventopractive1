package polymorphism;

public class Calling {
	public static void main(String[] args) {
		ChildC cc = new ChildC();
		System.out.println("calling by child object");
		cc.m1();
		cc.m2();
		System.out.println("===========");
		Parentp pp =new Parentp();
		System.out.println("calling by parent object");
		pp.m1();
		pp.m2();
		System.out.println("===========");
//		upcasting
		Parentp pc=new ChildC();
		System.out.println("calling by upcasted object");
		pc.m1();
		pc.m2();
		System.out.println("===========");
	}
}
