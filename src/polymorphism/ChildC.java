package polymorphism;

public class ChildC extends Parentp {
	@Override
	public void m1() {
//		super.m1();
		System.out.println("method m1 present in child class");
	}

	public void m2() {
//		super.m1();
		System.out.println("method m2 present in child class");
	}

}
