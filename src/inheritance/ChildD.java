package inheritance;

public class ChildD extends ChildC {

	public void m4() {
		System.out.println("childd class m4 method");
	}

	public static void main(String[] args) {
		ChildD cd = new ChildD();
		cd.m4();
		cd.m3();
		cd.m2();
		cd.m1();
	}
}
