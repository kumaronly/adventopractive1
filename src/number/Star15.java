package number;

import java.util.Scanner;

public class Star15 {
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int row = sc.nextInt();
        System.out.print("Enter the number of columns: ");
		 
        int columns = sc.nextInt();
        for(int i=row-1;i>=1;i--)
        {
        	for(int j=1;j<=row-i;j++)
        	{
        		System.out.print(" ");
        	}
        	for(int j=1;j<(i*2)-1;j++)
        	{
        		System.out.print("*");
        	}
        	System.out.println(" ");
        }
        for(int i=1;i<=row;i++)
        {
        	for(int j=1; j<=row-i; j++)
        	{
        		System.out.print(" ");
        	}
        	for(int j=1; j<=(i*2)-1;j++)
        	{
        		System.out.print("*");
        	}
        	System.out.println(" ");
        }
        

	}

}
