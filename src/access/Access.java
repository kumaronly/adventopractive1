package access;

public class Access {
	public int a = 10;
	private int pb = 20;
	protected int cc = 30;
	int dd = 40;

	public Access() {
		super();
	}

	public Access(int a, int pb, int cc, int dd) {
		super();
		this.a = a;
		this.pb = pb;
		this.cc = cc;
		this.dd = dd;
	}

	public int getPb() {
		return pb;
	}

	public void setPb(int pb) {
		this.pb = pb;
	}

	public void m1() {
		System.out.println("1st class");
	}
}
