package access;

class Parent{
	public String name1;

	public Parent(String name1) {
		super();
		this.name1 = name1;
	}
	
}

public class Chaning extends Parent {
	public String name2;
	
	public Chaning(String name2) {
		super("jitendra");
		this.name2 = name2;
	}

	public void m1(String name3) {
		System.out.println("inside the method "+name3);
		System.out.println("inside the same class    "+name2);
		System.out.println("inside the parent class "+name1);
	}
	public static void main(String[] args) {
		Chaning c= new Chaning("kumar");
		c.m1("jitu");
	}
}
