package com.adv.traing.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListEx {

	public void m1() {

		List<String> strList = new ArrayList<>();
		strList.add("kumar");
		strList.add("jitu");
		strList.add("ritu");
		strList.add("pooja");
		strList.add("deepu");

		System.out.println(strList);
		System.out.println("==============");
		strList.remove("deepu");
		System.err.println(strList);
		System.out.println("deepu is removed");
		System.out.println("================");

//		iteration using for loop
		for (int i = 0; i < strList.size(); i++) {
			String strValue = strList.get(i);
			System.out.println("String value is : " + strValue);
		}
		System.out.println("=======================");
//		iterator using iterator

		Iterator<String> iter = strList.iterator();
		while (iter.hasNext()) {
			String str = iter.next();
			System.out.println("string iter : " + str);
		}
		System.out.println("====================");
		ListIterator<String> strListIter = strList.listIterator();
		while (strListIter.hasNext()) {
			String str = strListIter.next();
			System.out.println("String list iter = " + str);
		}
		System.out.println("====================");
		for (String strVal : strList) {
			System.out.println("String for each = " + strVal);
		}
	}

}
