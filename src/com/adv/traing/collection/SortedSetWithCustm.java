package com.adv.traing.collection;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetWithCustm {
	private SortedSet<Student2> es = new TreeSet<Student2>();

	public void add(Student2 student2) {
		es.add(student2);
	}

	public void delete(Student2 student2) {
		es.remove(student2);
	}

	public void iterate() {
		Iterator<Student2> studIter = es.iterator();
		while (studIter.hasNext()) {
			Student2 student2 = studIter.next();
			System.out.println("student2 ========= " + student2);
		}
	}

}
