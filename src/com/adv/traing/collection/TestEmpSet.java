package com.adv.traing.collection;

public class TestEmpSet {

	public static void main(String[] args) {
		EmpSet empSet = new EmpSet();

		Emp e11 = new Emp(1, "varun", 30, "varun232@gmail.com");
		Emp e12 = new Emp(2, "manish", 25, "manish232@gmail.com");
		Emp e13 = new Emp(3, "shubham", 27, "shubham232@gmail.com");
		Emp e14 = new Emp(4, "varun", 27, "krishna232@gmail.com");
		Emp e15 = new Emp(5, "surender", 26, "surendra232@gmail.com");
		Emp e16 = new Emp(6, "mahesh", 25, "mahesh232@gmail.com");

		empSet.add(e11);
		empSet.add(e12);
		empSet.add(e13);
		empSet.add(e14);
		empSet.add(e15);
		empSet.add(e16);

		empSet.print();

		System.out.println("no of Object....> " + empSet.size());
		empSet.delete(e16);

		empSet.print();
		System.out.println("no of Object....> " + empSet.size());
	}

}
