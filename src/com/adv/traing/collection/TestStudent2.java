package com.adv.traing.collection;

import java.util.Date;

public class TestStudent2 {
	public static void main(String[] args) {

		Student2 s1 = new Student2(1, "kumar", 25, "kumar232@gmail.com", "kumar", new Date(), "kumar", new Date());
		Student2 s2 = new Student2(2, "madhu", 24, "madhu232@gmail.com", "madhu", new Date(), "madhu", new Date());
		Student2 s3 = new Student2(3, "Shanker", 23, "Shanker232@gmail.com", "Shanker", new Date(), "Shanker",
				new Date());
		Student2 s4 = new Student2(4, "prakash", 26, "prakash232@gmail.com", "prakash", new Date(), "prakash",
				new Date());
		Student2 s5 = new Student2(5, "Soumya", 28, "soumya232@gmail.com", "Soumya", new Date(), "Soumya", new Date());
		Student2 s6 = new Student2(6, "deepu", 29, "deepu232@gmail.com", "deepu", new Date(), "deepu", new Date());
		Student2 s7 = new Student2(7, "deepu", 28, "deepu232@gmail.com", "deepu", new Date(), "deepu", new Date());

		SortedSetWithCustm sortedSetWithCustm = new SortedSetWithCustm();
		sortedSetWithCustm.add(s1);
		sortedSetWithCustm.add(s2);
		sortedSetWithCustm.add(s3);
		sortedSetWithCustm.add(s4);
		sortedSetWithCustm.add(s5);
		sortedSetWithCustm.add(s6);
		sortedSetWithCustm.add(s7);

		sortedSetWithCustm.iterate();
	}
}
