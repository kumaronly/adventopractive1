package com.adv.traing.collection;

import java.util.ArrayList;
import java.util.List;

public class EmpSet {
	private List<Emp> empSet = new ArrayList();

	public void add(Emp emp) {
		empSet.add(emp);
	}

	public void delete(Emp emp) {
		empSet.remove(emp);
	}

	public void print() {
		System.out.println("Emplyee list in set -----------> " + empSet);
		;
		System.out.println("-----------------------------------------");
	}

	public int size() {
		return empSet.size();
	}
}
