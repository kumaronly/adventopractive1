package com.adv.traing.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetEx1 {
	private Set<Integer> intSet = new HashSet<>();

	public void add(Integer inputInt) {
		intSet.add(inputInt);
	}

	public void delete(Integer inputInt) {
		intSet.remove(inputInt);
	}

	public void iterate() {
		Iterator<Integer> inter = intSet.iterator();
		while (inter.hasNext()) {
			Integer intVal = inter.next();
			System.out.println("Integer value : " + intVal);
		}
	}
}
