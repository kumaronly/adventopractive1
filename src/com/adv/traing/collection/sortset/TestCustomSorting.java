package com.adv.traing.collection.sortset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestCustomSorting {
	public static void main(String[] args) {
		List<Student> list=new ArrayList<>();
		list.add(new Student("Sanjay",17,"sanjay@gmail.com"));
		list.add(new Student("Sankar",13,"Sankar@gmail.com"));
		list.add(new Student("Smruti",16,"Smruti@gmail.com"));
		list.add(new Student("Soumya",15,"Soumya@gmail.com"));
		list.add(new Student("Santosh",14,"Santosh@gmail.com"));
		list.add(new Student("Sibprasad",16,"Sibprasad@gmail.com"));
		
		System.out.println("Student list before sorting by Name---"+list);
		Collections.sort(list, new StudentNameSorting());
		System.out.println("Student list after sorting by Name---"+list);
		
		System.out.println("Student list before sorting by Email---"+list);
		Collections.sort(list, new StudentEmailSorting());
		System.out.println("Student list after sorting by Email---"+list);
		
		System.out.println("Student list before sorting by Age---"+list);
		Collections.sort(list, new StudentAgeSorting());
		System.out.println("Student list after sorting by Age---"+list);
		
		
	}
}
