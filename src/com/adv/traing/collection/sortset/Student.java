package com.adv.traing.collection.sortset;

public class Student implements Comparable<Student> {

	private String name;
	private int age;
	private String email;

	public Student(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", email=" + email + "]";
	}

	@Override
	public int compareTo(Student o) {
		return this.getAge()- o.getAge();
	}

}
