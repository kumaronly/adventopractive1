package com.adv.traing.collection.sortset;

import java.util.Comparator;

public class StudentEmailSorting implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		return o1.getEmail().compareTo(o2.getName());
	}

}
