package com.adv.traing.collection;

import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetEx {
	public static void main(String[] args) {
		SortedSet<String> ss = new TreeSet<>();
		ss.add("rahul");
//		 ss.add(null);
		ss.add("kunal");
		ss.add("rohit");
		ss.add("virat");
		ss.add("gill");
		ss.add("surya");
		ss.add("polad");
		ss.add("bhumra");
		ss.add("bhumra");

		System.out.println("Sorted set list " + ss);
	}
}
