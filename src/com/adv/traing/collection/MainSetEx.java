package com.adv.traing.collection;

public class MainSetEx {
//	private static SetEx setEx;

	public static void main(String[] args) {
		SetEx setEx = new SetEx();

		setEx.add("jitu");
		setEx.add("situ");
		setEx.add("ritu");
		setEx.add("mitu");
		setEx.add("geetu");

		setEx.iterate();

		setEx.delete("geetu");
		System.out.println("geeta is removed");
		setEx.iterate();
	}
}
