package com.adv.traing.collection;

public class TestEmpList {

	public static void main(String[] args) {
		
		EmpList empList=new EmpList();
		
		Emp e1=new Emp(1,"jitendra",25,"kumarjitu232@gmail.com");
		Emp e2=new Emp(2,"Surendra",26,"kumarsitu232@gmail.com");
		Emp e3=new Emp(3,"Rajendra",24,"kumarritu232@gmail.com");
		Emp e4=new Emp(4,"Smruti",23,"kumargeetu232@gmail.com");
		Emp e5=new Emp(5,"Soumya",25,"soumya232@gmail.com");
		Emp e6=new Emp(6,"Prakash",27,"prakash32@gmail.com");
		
		empList.add(e1);
		empList.add(e2);
		empList.add(e3);
		empList.add(e4);
		empList.add(e5);
		empList.add(e6);
		
		empList.print();
		
		System.out.println("no of Object....> "+empList.size());
		empList.delete(e6);
		
		empList.print();
		System.out.println("no of Object....> "+empList.size());
	}

}
