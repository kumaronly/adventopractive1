package com.adv.traing.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListEx1 {
	List<Integer> intList = new ArrayList<>();

	public void add() {
		intList.add(10);
		intList.add(20);
		intList.add(30);
		intList.add(40);
		intList.add(50);
		System.out.println(intList);
		System.out.println("===================");

	}

	public void delete() {
		intList.remove(4);
		System.out.println("50 is removed");
		System.out.println("===================");
	}

	public void forIterateMethod() {
		for (int i = 0; i < intList.size(); i++) {
			Integer intVal = intList.get(i);
			System.out.println("integer value is :" + intVal);
		}
		System.out.println("==================");
	}

	public void iteratorMethod() {
		Iterator<Integer> iter = intList.iterator();
		while (iter.hasNext()) {
			Integer inter = iter.next();
			System.out.println("integer iter : " + inter);
		}
		System.out.println("====================");
	}

	public void listIteratorMethod() {
		ListIterator<Integer> intListIter = intList.listIterator();
		while (intListIter.hasNext()) {
			Integer intr = intListIter.next();
			System.out.println("String list iter = " + intr);
		}
		System.out.println("====================");
		for (Integer intVal : intList) {
			System.out.println("Integer for each = " + intVal);
		}
		System.out.println("====================");
	}
}
