package com.adv.traing.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetEx {
	private Set<String> strSet = new HashSet<>();

	public void add(String inputStr) {
		strSet.add(inputStr);
	}

	public void delete(String inputStr) {
		strSet.remove(inputStr);
	}

	public void iterate() {
		Iterator<String> iter = strSet.iterator();
		while (iter.hasNext()) {
			String strval = iter.next();
			System.out.println("String value : " + strval);
		}
	}

}
