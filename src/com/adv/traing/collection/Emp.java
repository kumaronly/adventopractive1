package com.adv.traing.collection;

import java.util.Objects;

public class Emp {
	private int id;
	private String name;
	private int age;
	private String email;

	public Emp(int id, String name, int age, String email) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", age=" + age + ", email=" + email + "]";
	}

//	compare complete content
	@Override
	public int hashCode() {
		String value=this.getId() +this.getName() +this.getAge() +this.getEmail();
		int hashCodeVal= value.hashCode();
		System.out.println("HashCode Value = "+hashCodeVal);
		System.out.println("******************");
		return hashCodeVal;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getName().equals(((Emp)obj).getName())) 
		{
			if(this.getId()== ((Emp)obj).getId())
			{
				if(this.getAge() == ((Emp)obj).getAge())
				{
					if(this.getEmail().equals(((Emp)obj).getEmail()))
							{
								System.out.println("Inside the equlas .... the value is "+ true);
								System.out.println("--------------");
								return true;
							}
				}
			}
		}
		System.out.println("Inside the equlas .... the value is "+ false);
		System.out.println("--------------");
		return false;
	}
	
	

//	compare name
//	@Override
//	public int hashCode() {
//		return this.getName().hashCode();
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this.getName() ==((Emp) obj).getName()) {
//			return true;
//		}
//		return false;
//		
//	}
	
//	compare Email
//	@Override
//	public int hashCode() {
//		return this.getEmail().hashCode();
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this.getEmail() ==((Emp) obj).getEmail()) {
//			return true;
//		}
//		return false;
//		
//	}

//	compare id
//	@Override
//	public int hashCode() {
//		return Objects.hash(age, email, id, name);
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this.getId() == ((Emp) obj).getId()) {
//			return true;
//
//		}
//		return false;
//
//	}
	
	

}
