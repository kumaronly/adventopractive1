package com.adv.exeption;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TryResourses17v {
	public static void main(String[] args) {
		try(BufferedReader br =new BufferedReader(new FileReader("input.txt"))){
//			use br based on our requirment 
//			once controll end of try block automatically closed 
//			we are not required to closed resources
			
		}
		catch(IOException io)
		{
			io.printStackTrace();
		}
	}
}
//we are not required to write finally block
//so complexcity is decrese
//length of code will be reduce
//readabily is improved