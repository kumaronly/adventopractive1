package com.adv.autoclosed;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializableEx {
	public void serialize() {
		Singleton singleton = Singleton.getInstance();
		System.out.println("before serialization....."+ singleton);
		FileOutputStream file =null;
		ObjectOutputStream out=null;
		try {
			file = new FileOutputStream(null)
		}
	}
}

class Emp implements Serializable {
	private String name;
	private int age;
	private String email;

	public Emp(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
