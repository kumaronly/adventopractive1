package com.adv.autoclosed;

public class TestAutoClosableEx {
	public static void main(String[] args) {
		try (AutoClosableEx autoClosableEx = new AutoClosableEx()) {
			autoClosableEx.m1();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class AutoClosableEx implements AutoCloseable {
	public void m1() {
		System.out.println("inside the m1");
	}

	@Override
	public void close() throws Exception {
		System.out.println("close method invoked...");
	}
}
