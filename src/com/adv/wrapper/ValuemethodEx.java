package com.adv.wrapper;

public class ValuemethodEx {
	public static void main(String[] args) {
		
//		converting object to primitive datatype
		
		Long i=20l;
		byte b=i.byteValue();
		short s=i.shortValue();
		int in=i.intValue();
		long l=i.longValue();
		double d=i.doubleValue();
		float fl=i.floatValue();
		System.out.println(b);
		System.out.println(s);
		System.out.println(in);
		System.out.println(l);
		System.out.println(d);
		System.out.println(fl);
	}
}
