package com.adv.wrapper;

public class ParseMethodEx {
	public static void main(String[] args) {
		String s="720";
		byte b=Byte.parseByte(s);
		short st=Short.parseShort(s);
		int i=Integer.parseInt(s,2);
		long l=Long.parseLong(s);
		double d=Double.parseDouble(s);
		float ft=Float.parseFloat(s);
		System.out.println(i);
	}
}
