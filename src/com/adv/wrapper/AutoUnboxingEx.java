package com.adv.wrapper;

public class AutoUnboxingEx {
	public static void main(String[] args) {
		Integer intr = 10;
		int i = intr;
		System.out.println(intr);
		System.out.println(i);
	}
}
