package com.adv.wrapper;

public class valueofEx {
	public static void main(String[] args) {
		byte b = 10;
		short st = 20;
		int i = 30;
		double d = 40.0;
		float f = 50.0f;
		char c = 'c';
		boolean bl = true;
		
//		converting to primitive to object explicitly
		Byte by = Byte.valueOf(b);
		Short sh = Short.valueOf(st);
		Integer intr = Integer.valueOf(i);
		Double db = Double.valueOf(d);
		Float fl = Float.valueOf(f);
		Character ch = Character.valueOf(c);
		Boolean bln = Boolean.valueOf(bl);
		String str = String.valueOf(b);
		
//		you can conver any datatype to string 
		
		System.out.println(by+" "+sh+" "+intr+" "+db+" "+fl+" "+ch+" "+bln+" "+str);

	}
}
