package com.adv.wrapper;

public class AutoBoxingEx {
	public static void main(String[] args) {
		int i = 10;
		Integer intr = i;
		
		System.out.println(i);
		System.out.println(intr);
	}
}
