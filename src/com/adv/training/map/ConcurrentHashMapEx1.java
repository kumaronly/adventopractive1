package com.adv.training.map;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapEx1 {
	public static void main(String[] args) {
		ConcurrentHashMap<String, String> chm = new ConcurrentHashMap();
		chm.putIfAbsent("a", "one");
		chm.putIfAbsent("b", "two");
		chm.putIfAbsent("c", "three");
		chm.putIfAbsent("d", "four");
		chm.putIfAbsent("e", "five");
		System.out.println(chm);
	}
}
