package com.adv.training.map;

public class Student {
	private int id;
	private String name;
	private int age;
	private String email;
	private String standard;
	private String section;

	public Student(int id, String name, int age, String email, String standard, String section) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.email = email;
		this.standard = standard;
		this.section = section;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", email=" + email + ", standard=" + standard
				+ ", section=" + section + "]";
	}

}
