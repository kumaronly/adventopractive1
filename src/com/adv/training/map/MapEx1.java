package com.adv.training.map;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapEx1 {
	public static void main(String[] args) {
		MapEx1 ex1=new MapEx1();
		ex1.iterateMap();
		System.out.println("--------------");
		ex1.IterateMapGetKeys();
		System.out.println("--------------");
		ex1.IterateMapGetValues();
	}
	public Map<Integer, String> getMap()
	{
		Map<Integer, String> mp= new HashMap<>();
		mp.put(1, "honda shine");
		mp.put(2, "honda Unikon");
		mp.put(3, "honda Hornet");
		mp.put(4, "honda CBR");
		return mp;
	}
	public void iterateMap()
	{
		Map<Integer, String> mp= getMap();
		Set<Entry<Integer, String>> entrySet= mp.entrySet();
		Iterator<Entry<Integer, String>> entryIter =entrySet.iterator();
		while(entryIter.hasNext())
		{
			Entry<Integer, String> entry= entryIter.next();
			System.out.println("key= "+entry.getKey()+", value="+entry.getValue());
		}
	}
	public void IterateMapGetKeys() {
		Map<Integer, String> mp=getMap();
		Set<Integer> entrySet=mp.keySet();
		Iterator<Integer> entryIter=entrySet.iterator();
		while(entryIter.hasNext())
		{
			Integer key=entryIter.next();
			System.out.println("key= "+key+", value="+mp.get(key));
		}
	}
	public void IterateMapGetValues() {
		Map<Integer, String> mp=getMap();
		Collection<String> entrySet=mp.values();
		Iterator<String> entryIter=entrySet.iterator();
		while(entryIter.hasNext())
		{
			String value=entryIter.next();
			System.out.println("value= "+value);
		}
	}
}
