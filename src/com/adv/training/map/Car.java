package com.adv.training.map;

public class Car {
	private int year;
	private String name;
	private String color;
	private int price;

	public Car(int year, String name, String color, int price) {
		super();
		this.year = year;
		this.name = name;
		this.color = color;
		this.price = price;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Car [year=" + year + ", name=" + name + ", color=" + color + ", price=" + price + "]";
	}

}
