package com.adv.training.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapWithCustomValue {
	public static void main(String[] args) {
		Map<Integer, List<Car>> carMap = new HashMap<>();

		CarJdbc carJdbc = new CarJdbc();
		List<Car> carList = carJdbc.getCar();
		Iterator<Car> carIter = carList.iterator();
		while (carIter.hasNext()) {
			Car car = carIter.next();
			if (carMap.containsKey(car.getPrice())) {
				List<Car> carListExisting = carMap.get(car.getPrice());
				carListExisting.add(car);
			} else {
				List<Car> carValList = new ArrayList<>();
				carValList.add(car);
				carMap.put(car.getPrice(), carValList);

			}
		}
		System.out.println(carMap);
		System.out.println(carMap.get(5000000));
	}
}
