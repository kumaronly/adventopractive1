package com.adv.training.map;

import java.util.HashMap;
import java.util.Map;

public class MapEx {
	public static void main(String[] args) {
		Map<Integer, String> mp = new HashMap<>();
		mp.put(1, "kumar");
		mp.put(2, "deebakar");
		mp.put(3, "rabindra");
		mp.put(4, "susma");

		System.out.println(mp.get(1));
		System.out.println(mp.get(3));
	}
}
