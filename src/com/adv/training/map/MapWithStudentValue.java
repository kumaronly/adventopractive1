package com.adv.training.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapWithStudentValue {
	public static void main(String[] args) throws ClassCastException {
		Map<String, Map<String, List<Student>>> studentMap = new HashMap<>();
		StudentJdbc studentJdbc = new StudentJdbc();

		List<Student> studentList = studentJdbc.getStudents();
		Iterator<Student> studentIter = studentList.iterator();
		while (studentIter.hasNext()) {
			Student student = studentIter.next();
			if (studentMap.containsKey(student.getStandard())) 
			{
				Map<String, List<Student>> studentSecListmap = studentMap.get(student.getStandard());
				if (studentSecListmap.containsKey(student.getSection())) 
				{
					List<Student> studentListExisting = studentSecListmap.get(student.getSection());
					studentListExisting.add(student);
				} else {
					List<Student> studentNew = new ArrayList<>();
					studentNew.add(student);
					studentSecListmap.put(student.getSection(), studentNew);
				}
			} else {
				List<Student> studentValList = new ArrayList<>();
				studentValList.add(student);
				Map<String, List<Student>> studentSecListmap = new HashMap<>();
				studentSecListmap.put(student.getSection(), studentValList);
				studentMap.put(student.getStandard(),studentSecListmap);
			}
			System.out.println(studentMap);
			System.out.println(studentMap.get("9th"));
		}

	}
}
