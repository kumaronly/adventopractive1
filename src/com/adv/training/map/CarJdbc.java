package com.adv.training.map;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CarJdbc {
	public List<Car> getCar() {
		List<Car> cars = new ArrayList<>();
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/training-08", "root", "root");
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from car");
			while (rs.next()) {
				int year = rs.getInt("year");
				String name = rs.getString("name");
				String color = rs.getString("color");
				int price = rs.getInt("price");

				Car car = new Car(year, name, color, price);
				cars.add(car);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cars;
	}
}
